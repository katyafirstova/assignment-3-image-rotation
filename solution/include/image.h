#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>


struct image {
    uint32_t width;
    uint32_t height;
    struct pixel *data;
};

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image image_init (const uint32_t width, const uint32_t height);

void image_destroy(struct image* image);

void image_set_pixel (struct image* image, struct pixel pix, uint32_t i, uint32_t j);

struct pixel image_get_pixel (const struct image* image, uint32_t i, uint32_t j);


#endif 





