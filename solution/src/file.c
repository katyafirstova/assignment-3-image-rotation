#include "file.h"
#include <stdio.h>



bool open_file(FILE** file, const char* path,  const char* mode) {
    if(!path) return false;
    *file = fopen(path, mode);
     return (*file != NULL);
}
 

bool close_file(FILE* file) {
    if (file) return !fclose(file);
    else return false;


}

