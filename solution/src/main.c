#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotation.h"
#include "status.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    if (argc != 3) { fprintf(stderr, "Error: two arguments expected"); return 1; }

    FILE* input = NULL;
    FILE* output = NULL;
    struct image image = {0};
    struct image rotated = {0};

  
    if (!(open_file(&input, argv[1], "rb"))) {
        fprintf(stderr, "Error: can't read input file");
    }

    if(!(open_file(&output, argv[2], "wb"))) {
        fprintf(stderr, "Error: can't write input file");
    }

    if(from_bmp(input, &image) != READ_OK) {
        fprintf(stderr, "Error: can't read bmp file");    
    }

    rotated = rotate(image);

    if(to_bmp(output, &rotated) != WRITE_OK) {
        fprintf(stderr, "Error: can't write output file");    
    }

    image_destroy(&image);
    image_destroy(&rotated);

    if (close_file(output)) fprintf(stderr, "Error: can't close output file");
    if (close_file(input))  fprintf(stderr, "Error: can't close input file");

  

  
    return 0;

}




   
