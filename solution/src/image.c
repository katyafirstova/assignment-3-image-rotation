#include "image.h"
#include <stdlib.h>


struct image image_init (const uint32_t width, const uint32_t height) {
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc (sizeof (struct pixel) * width * height);
    return image;
}

void image_destroy(struct image* image) {
    free(image->data);
}

struct pixel image_get_pixel(const struct image* image, uint32_t i, uint32_t j) {
    struct pixel pix = image->data[i + (j * image->width)];
    return pix;
}

void image_set_pixel(struct image* image, struct pixel pix, uint32_t i, uint32_t j) {
    image->data[i + (j * image->width)] = pix;
    
}


