#include "rotation.h"
#include "image.h"
    
    
struct image rotate( struct image source ) {

struct image result = image_init(source.height, source.width);
    
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            struct pixel pix = image_get_pixel(&source, j, i);
            image_set_pixel(&result, pix, result.width - i - 1, j);
            
    }
}
    
return result;

}



