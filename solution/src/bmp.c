#include "bmp.h"
#include "image.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>


#define BF_TYPE 0x4D42
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static void calculate_padding ( FILE* in, struct image* img) {
    if ((img->width * 3) % 4 != 0) fseek(in, 4 - ((img->width * 3) % 4), SEEK_CUR);
    
}

static bool create_image (FILE* in, const struct bmp_header header, struct image* img) {
    *img = image_init(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    for (int32_t i = 0; i < img->height; i++) {
        for (int32_t j = 0; j < img->width; j++) {
            struct pixel pixel = {0};
            if (fread(&pixel, sizeof (struct pixel), 1, in) == 0) {
                image_destroy(img);
                return false;
            }
            image_set_pixel (img, pixel, j, i);
        }
        calculate_padding(in, img);
    }
    return true;
}


static struct bmp_header create_header(struct image const* image) {
    struct bmp_header bmp_header = {0};
    bmp_header.bfType = BF_TYPE,
    bmp_header.biWidth = image->width,
    bmp_header.biHeight = image->height,
    bmp_header.bfileSize = B_OFF_BITS + image->width * image->height * sizeof(struct pixel)
    + image->height * ((image->width * 3) % 4),
    bmp_header.biSizeImage = bmp_header.bfileSize - B_OFF_BITS,
    bmp_header.bOffBits = B_OFF_BITS,
    bmp_header.biSize = BI_SIZE,
    bmp_header.biPlanes = BI_PLANES,
    bmp_header.biBitCount = BI_BIT_COUNT,
    
    bmp_header.bfReserved = 0,
    bmp_header.biXPelsPerMeter = 0,
    bmp_header.biYPelsPerMeter = 0,
    bmp_header.biClrUsed = 0,
    bmp_header.biClrImportant = 0,
    bmp_header.biCompression = 0;
    return bmp_header;
    
    
}

enum read_status from_bmp( FILE* in, struct image* image ) {
    if(in == NULL) return READ_ERROR;

    struct bmp_header bmp_header = {0};

    if (fread( &bmp_header, sizeof( struct bmp_header ), 1, in ) != 0)  {
        if(bmp_header.bfType == BF_TYPE) {
            if(create_image(in, bmp_header, image)) {
                return READ_OK;
            }
        } else { return READ_INVALID_HEADER_SIGNATURE; }
    } else {return READ_ERROR;}
    
    return READ_INVALID_HEADER_BIT_PER_PIXEL;
}

enum write_status to_bmp(FILE *out, struct image const* image) {
    if(out == NULL) return WRITE_ERROR;

    struct bmp_header bmp_header = create_header(image);
    
    if (fwrite(&bmp_header, sizeof(bmp_header), 1, out) != 0) {
        for (int32_t i = 0; i < image->height; i++){
            for (int32_t j = 0; j < image->width; j++) {
                struct pixel pixel = image_get_pixel (image, j, i);
                if (fwrite(&pixel, sizeof (struct pixel), 1, out) == 0) {
                    return WRITE_FWRITE_ERROR;
                }

            }  
            int8_t zero = 0;
            if ((image->width * 3) % 4 != 0) {
                for (size_t k = 0; k < 4 - ((image->width * 3) % 4); k++){
                    if(fwrite(&zero, sizeof (int8_t), 1, out) == 0) {
                        return WRITE_FWRITE_ERROR;
                    }
                }
            }
        }
    }  else {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}







