#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "status.h"
#include <stdio.h>



enum read_status from_bmp( FILE* in, struct image* image);

enum write_status to_bmp(FILE *out, struct image const* image);

#endif
